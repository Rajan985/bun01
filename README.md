# Elysia with Bun runtime

## Getting Started

Before getting the project make sure you have installed bun runtime in your machine.
Check their docs to setup bun in your machine at https://bun.sh/

After installing bun, run

```bash
bun install
```

You need to manually configure your database according to the project

## Development

To start the development server run:

```bash
bun run dev
```

## Using Docker

If you have docker installed in your machine, siple run:

```bash
docker-compose up --build
```

and your application will be up and running.

To access database open http://localhost:8081

For proper api docs' open http://localhost:3000/swagger
