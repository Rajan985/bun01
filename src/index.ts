import {Elysia} from "elysia";
import {swagger} from "@elysiajs/swagger";
import userRouter from "./routes/user.route";

const app = new Elysia().onError((ctx) => {
  return new Response(JSON.stringify({message: ctx.error.message}), {
    headers: {
      "Content-Type": "application/json",
    },
    status: ctx.set.status as number,
  });
});

app.use(swagger());

app.group("/v1/users", userRouter);

export default app;
