import bcrypt from "bcryptjs";

export const hashPassword = async (passowrd: string) =>
  bcrypt.hash(passowrd, 10);

export const checkPassword = async (
  hashedPassword: string,
  plainTextPassword: string
) => bcrypt.compare(plainTextPassword, hashedPassword);
