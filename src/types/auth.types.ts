import {SetCookieOptions} from "@elysiajs/cookie";
import {JWTPayloadSpec} from "@elysiajs/jwt";
import {Context} from "elysia";
import {JwtSign, JwtVerify} from "./jwt.types";

export interface LoginCustomContext extends Context {
  jwt: JwtSign & JwtVerify;

  setCookie: (name: string, value: string, options?: SetCookieOptions) => void;
}

export interface MeCustomContext extends Context {
  store: {
    username?: string;
    email?: string;
    password?: string;
    created_at?: string;
    updated?: string;
  };
}
