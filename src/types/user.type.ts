import {Context} from "elysia";
import {Document, ObjectId} from "mongodb";
import {JwtVerify} from "./jwt.types";

export interface ImageUploadFormData {
  image: Blob;
}

export interface UserDetalis extends Document {
  username: string;
  password: string;
  confirm_password: string;
  email: string;
  created_at: Date;
  updated_at: Date;
}

export interface CustomImageUploadContext extends Context {
  params: Record<"id", string>;
  body: unknown | ImageUploadFormData;
}
