import {Cookie} from "elysia";
import {HTTPStatusName} from "elysia/dist/utils";
import {JwtVerify} from "./jwt.types";

interface AuthHookContext {
  body: unknown;
  query: Record<string, string | null>;
  params: Record<never, string>;
  headers: Record<string, string | null>;
  cookie: Record<string, Cookie<unknown>>;
  set: {};
  path: string;
  request: Request;
  store: {};
}

export interface CustomIsAuthenticatedContext extends AuthHookContext {
  set: {
    status?: number | HTTPStatusName;
  };

  store: {
    username?: string;
    email?: string;
    created_at?: Date;
    updated_at?: Date;
  };

  jwt: JwtVerify;
}

export interface CustomControlResponseContext extends AuthHookContext {
  store: {
    username?: string;
    email?: string;
    password?: string;
    created_at?: Date;
    updated_at?: Date;
  };

  set: {
    status?: number | HTTPStatusName;
  };

  response?:
    | unknown
    | {
        message: string;
      };
}
