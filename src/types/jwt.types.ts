import {JWTPayloadSpec} from "@elysiajs/jwt";

export interface JwtSign {
  sign: (
    morePayload: Record<string, string> & JWTPayloadSpec
  ) => Promise<string>;
}

export interface JwtVerify {
  verify: (
    jwt?: string
  ) => Promise<false | (Record<string, string> & JWTPayloadSpec)>;
}
