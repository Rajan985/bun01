import {Context, Elysia, t} from "elysia";
import Auth from "../controllers/auth.controller";
import {isAuthenticated, controlUserData} from "../hooks/auth.hook";
import {jwt} from "@elysiajs/jwt";
import {cookie} from "@elysiajs/cookie";
import {
  signupBodyValidator,
  loginBodyValidator,
} from "../validators/auth.validator";
import User from "../controllers/user.controller";
import {imageUploadValidtor} from "../validators/user.validator";
const auth = new Auth();
const user = new User();

const router = (route: Elysia<"/v1/users">) =>
  route
    .use(
      jwt({
        name: "jwt",
        secret: process.env.JWT_PUBLIC_KEY!,
      })
    )
    .use(cookie())

    .post("/signup", auth.signup, {
      ...signupBodyValidator,
    })

    .post("/login", auth.login, {
      ...loginBodyValidator,
    })

    .guard(
      {
        beforeHandle: [isAuthenticated],
      },
      (route) =>
        route
          .get("/me", auth.me, {
            afterHandle: [controlUserData],
          })
          .patch("/id/:id", () => {
            return {
              message: "User update",
            };
          })

          .post("/id/:id/image", user.uploadImage, {...imageUploadValidtor})
    );

export default router;
