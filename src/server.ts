import app from ".";

import {Db, MongoClient} from "mongodb";

const dbName: string = process.env.DB_NAME!;
const dbUrl: string = process.env.DB_URL!;
const client: MongoClient = new MongoClient(dbUrl);

client.connect().then((res) => {
  console.log("DB CONNECTED SUCCESSFULLY");
});

const port = process.env.PORT ?? 3000;

const server = app.listen(port);

console.log(
  `🦊 Elysia is running at ${server.server?.hostname}:${server.server?.port}`
);

export const db: Db = client.db(dbName);
