import {
  CustomControlResponseContext,
  CustomIsAuthenticatedContext,
} from "../types/authHooks.type";
import {db} from "../server";
import {ObjectId, WithId} from "mongodb";
import {UserDetalis} from "../types/user.type";

export const isAuthenticated = async (
  context: CustomIsAuthenticatedContext
) => {
  if (
    !context.cookie.auth ||
    (context.cookie.auth &&
      (context.cookie.auth as unknown as string).split(" ")[0] !== "Bearer")
  ) {
    context.set.status = 401;

    throw new Error("You are not authenticated");
  }

  const decodedData = await context.jwt.verify(
    (context.cookie.auth as unknown as string).split(" ")[1]
  );

  if (!decodedData) {
    context.set.status = 401;

    throw new Error("You are not authenticated");
  }

  const user = <WithId<UserDetalis>>(
    await db.collection("Users").findOne({_id: new ObjectId(decodedData.id)})
  );

  context.store = user;
};

export const controlUserData = (context: CustomControlResponseContext) => {
  if (`${context.set.status}`.startsWith("4")) {
    return {
      message: (context.response as {message: string}).message,
    };
  }
  const user = context.store;

  delete user.password;
  return user;
};
