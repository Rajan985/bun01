import {Context} from "elysia";
import {
  CustomImageUploadContext,
  ImageUploadFormData,
  UserDetalis,
} from "../types/user.type";
import path from "path";
import sharp from "sharp";
import {ObjectId, WithId} from "mongodb";
import {db} from "../server";

export default class User {
  constructor() {}

  async uploadImage(ctx: CustomImageUploadContext) {
    let chunks = await (ctx.body as ImageUploadFormData).image.arrayBuffer();

    const buf = Buffer.from(chunks);

    const user = <WithId<UserDetalis>>(
      await db.collection("Users").findOne({_id: new ObjectId(ctx.params.id)})
    );

    const dir = path.join(
      __dirname,
      "../images",
      `/${user.username}-image.png`
    );

    await sharp(buf).resize(400, 400).toFile(dir);

    ctx.set.status = 202;
    return {
      message: "Successfully Queued",
    };
  }
}
