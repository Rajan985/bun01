import {Context} from "elysia";
import {db} from "../server";
import {WithId} from "mongodb";
import {UserDetalis} from "../types/user.type";
import {hashPassword, checkPassword} from "../utils/bcrypt.util";
import {LoginCustomContext, MeCustomContext} from "../types/auth.types";

export default class Auth {
  constructor() {}

  async signup(ctx: Context) {
    const {set, body} = ctx;

    const {username, email, password, confirm_password} = body as UserDetalis;

    if (password !== confirm_password) {
      set.status = 400;

      throw new Error("Password fields did not match");
    }

    const user = <WithId<UserDetalis>[]>await db
      .collection("Users")
      .find({$or: [{username}, {email}]})
      .toArray();

    if (user.length > 0) {
      set.status = 400;

      throw new Error("Username or email alreaddy taken.");
    }
    const hashedPassword = await hashPassword(password);

    await db.collection("Users").insertOne({
      username,
      password: hashedPassword,
      email,
      created_at: new Date(),
      updated_at: new Date(),
    });

    set.status = 201;
    return {
      message: "You're signed up",
    };
  }

  async login(ctx: LoginCustomContext) {
    const {body, set} = ctx;

    const {username, password} = body as UserDetalis;

    const user = <WithId<UserDetalis>>(
      await db.collection("Users").findOne({username})
    );

    if (!user || !(await checkPassword(user.password, password))) {
      set.status = 401;

      throw new Error("Username or Password did not match.");
    }
    ctx.setCookie(
      "auth",
      "Bearer " + (await ctx.jwt.sign({id: user._id as unknown as string})),
      {
        httpOnly: process.env.COOKIE_SETUP_STATUS === "true" ? true : false,
        maxAge: 7 * 86400,
        secure: process.env.COOKIE_SETUP_STATUS === "true" ? true : false,
      }
    );

    set.status = 200;

    return {
      message: "You are logged-in",
      token: ctx.cookie.auth,
    };
  }

  async me(ctx: MeCustomContext) {}
}
