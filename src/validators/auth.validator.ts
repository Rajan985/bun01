import {t} from "elysia";

export const signupBodyValidator = {
  body: t.Object({
    username: t.String(),
    email: t.String(),
    password: t.String(),
    confirm_password: t.String(),
  }),
};

export const loginBodyValidator = {
  body: t.Object({
    username: t.String(),
    password: t.String(),
  }),
};
