import {t} from "elysia";

export const imageUploadValidtor = {
  body: t.Object({
    image: t.File({
      type: ["image/jpeg", "image/png"],
    }),
  }),
};
